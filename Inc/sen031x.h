/*
 * sen031x.h
 *
 *  Created on: 6 de mai de 2020
 *      Author: oscar
 */

#ifndef SEN031X_H_
#define SEN031X_H_

#include "main.h"

#include <math.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

UART_HandleTypeDef huart2;


#define DIST_MIN			   30 	//30mm
#define DIST_MAX			   250 	//250mm



int fn_get_sen031x();


void insertionSort(float vetor[], int tamanhoVetor);


#endif /* SEN031X_H_ */
